/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.ui.webui.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static org.junit.Assert.*;

/**
 *
 * @author mirash
 */
public class LoginPage extends BasePage {

    private static final int MSG_WAIT_IN_SEC = 3;

    public LoginPage(WebDriver driver, String baseUrl) {
        super(driver, baseUrl);
    }

    public void login(String login, String pass) {
        WebElement userField = driver.findElement(By.name("j_username"));
        userField.sendKeys(login);

        WebElement passField = driver.findElement(By.name("j_password"));
        passField.sendKeys(pass);
    }

    public void submit() {
        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click();
    }

    public void checkErrorMessage(String msgExpected) {
        String errorMsgXPath = "//p/font";
        WebElement msgElement = driver.findElement(By.xpath(errorMsgXPath));
        assertNotNull(msgElement);

        (new WebDriverWait(driver, MSG_WAIT_IN_SEC)).until(
                ExpectedConditions.textToBePresentInElement(
                msgElement,
                msgExpected));
    }

    @Override
    protected Page getNext() throws Exception {
        this.waitForLoad();
        return new ControlPage(driver, baseUrl);
    }

    @Override
    protected void crawl() throws Exception {
        driver.get(baseUrl + getRelative());

        this.validateCurrentPage();

        this.login("rusakovich", "12345678");
        submit();
    }

    @Override
    public String getRelative() {
        return "spring_security_login";
    }
}
