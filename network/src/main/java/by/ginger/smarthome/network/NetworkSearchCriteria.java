/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.network;

import by.ginger.smarthome.provider.device.DeviceType;

/**
 *
 * @author rusakovich
 */
public class NetworkSearchCriteria {

    private String address;
    private String label;
    private DeviceType deviceType;
    private boolean isSignaling;

    public NetworkSearchCriteria() {
    }

    public NetworkSearchCriteria(String address, String label, DeviceType deviceType, boolean isSignaling) {
        this.address = address;
        this.label = label;
        this.deviceType = deviceType;
        this.isSignaling = isSignaling;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(DeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public boolean isSignaling() {
        return isSignaling;
    }

    public void setSignaling(boolean isSignaling) {
        this.isSignaling = isSignaling;
    }

}
