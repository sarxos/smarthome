/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.network;

import by.ginger.smarthome.network.exception.NetworkException;
import by.ginger.smarthome.provider.device.Device;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author mirash
 */
public abstract class AbstractStorableNetworkManager implements NetworkManager, NetworkManagerStorable {

    protected final List<Device> devices = Collections.synchronizedList(new LinkedList<Device>());

    @Override
    public List<Device> search(NetworkSearchCriteria criteria) throws NetworkException {
        List<Device> suitable = new LinkedList<>();

        for (Device device : devices) {
            if (criteria.getAddress() != null && !device.getAddress().equals(criteria.getAddress())) {
                continue;
            }

            if (criteria.getDeviceType() != null && !device.getDeviceType().equals(criteria.getDeviceType())) {
                continue;
            }

            if (criteria.getLabel() != null && !device.getLabel().equalsIgnoreCase(criteria.getLabel())) {
                continue;
            }

            suitable.add(device);
        }

        return suitable;
    }
    
    @Override
    public void setStorage(NetworkManagerStorage storage){
        storage.add(this);
    }
}
