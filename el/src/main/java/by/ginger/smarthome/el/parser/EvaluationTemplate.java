/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package by.ginger.smarthome.el.parser;

/**
 *
 * @author mirash
 */
public interface EvaluationTemplate {

    public String getEvaluationStart();

    public String getEvaluationEnd();
}
